# NRV2E-csharp

Implementation of the NRV2E decompression algorithm using C# programming language.

Copyright (c) 2018 Bartosz Soja <bartosz.soja@gmail.com>

Based on original UCL library created by:
Markus F.X.J. Oberhumer <markus@oberhumer.com>
http://www.oberhumer.com/opensource/ucl/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

----

Library is hosted on NuGet and there you can find latest build.
```
https://www.nuget.org/packages/NRV2E/
```

----

To build:
```
dotnet restore src/NRV2E.csproj
msbuild /p:Configuration=Release /t:Clean,Build src/NRV2E.csproj
```

To create NuGet package:
```
nuget pack NRV2E.nuspec
```
