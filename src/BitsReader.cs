/*  
    Helper class for working with bits 
    used by NRV2E decompression algorithm implementation

    Copyright (c) 2018 Bartosz Soja <bartosz.soja@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

using System;

namespace NRV2E
{
    public class BitsReader
    {
        private byte[] data;
        private int dataPosition;
        private int currentByte;
        private int bitPosition;

        public BitsReader(byte[] data, int startIdx = 0)
        {
            this.data = data;
            dataPosition = startIdx;
        }

        public bool IsDataAvailable
        {
            get { return dataPosition < data.Length; }
        }

        public byte ReadBit()
        {
            if (!IsDataAvailable) throw new Exception("No more data available!");

            if (bitPosition == 0)
            {
                currentByte = data[dataPosition++] & (0xff);
                bitPosition = 8;
            }
            return (byte) (((uint) currentByte >> --bitPosition) & 1);
        }

        public byte ReadByte()
        {
            if (!IsDataAvailable) throw new Exception("No more data available!");

            return (byte) (data[dataPosition++] & (0xff));
        }
    }
}