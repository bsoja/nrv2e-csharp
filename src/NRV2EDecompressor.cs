/*  
    Implementation of the NRV2E decompression algorithm

    Copyright (c) 2018 Bartosz Soja <bartosz.soja@gmail.com>

    Based on original UCL library created by:
    Markus F.X.J. Oberhumer <markus@oberhumer.com>
    http://www.oberhumer.com/opensource/ucl/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

using System;

namespace NRV2E
{
    public class NRV2EDecompressor
    {
        public byte[] Decompress(byte[] src)
        {
            // first 4 bytes are output size
            var bitsReader = new BitsReader(src, 4);
            var outputSize = BitConverter.ToInt32(src, 0);
            var dst = new byte[outputSize];
            uint olen = 0;
            uint last_m_off = 1;

            for (; ; )
            {
                uint m_off = 1;
                uint m_len;

                if (!bitsReader.IsDataAvailable) return dst;

                while (bitsReader.ReadBit() == 1)
                {
                    dst[olen++] = bitsReader.ReadByte();
                }
                for (; ; )
                {
                    if (!bitsReader.IsDataAvailable) return dst;

                    m_off = m_off * 2 + bitsReader.ReadBit();

                    if (bitsReader.ReadBit() == 1) break;
                    m_off = (m_off - 1) * 2 + bitsReader.ReadBit();
                }
                if (m_off == 2)
                {
                    m_off = last_m_off;
                    m_len = bitsReader.ReadBit();
                }
                else
                {
                    m_off = (m_off - 3) * 256 + bitsReader.ReadByte();
                    if (m_off == 1)
                    {
                        break;
                    }
                    m_len = (m_off ^ 1) & 1;
                    m_off >>= 1;
                    last_m_off = ++m_off;
                }

                if (!bitsReader.IsDataAvailable) return dst;

                if (m_len > 0)
                {
                    m_len = (uint)1 + bitsReader.ReadBit();
                }
                else if (bitsReader.ReadBit() == 1)
                {
                    m_len = (uint)3 + bitsReader.ReadBit();
                }
                else
                {
                    m_len++;
                    do
                    {
                        if (!bitsReader.IsDataAvailable) return dst;
                        m_len = m_len * 2 + bitsReader.ReadBit();
                        if (!bitsReader.IsDataAvailable) return dst;
                    } while (bitsReader.ReadBit() == 0);
                    m_len += 3;
                }
                m_len += (uint)(m_off > 0x500 ? 1 : 0);

                var m_pos = (int)(olen - m_off);
                dst[olen++] = dst[m_pos++];
                do dst[olen++] = dst[m_pos++]; while (--m_len > 0);
            }

            return dst;
        }
    }
}